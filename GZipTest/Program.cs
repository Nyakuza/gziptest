﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;

public class FileCompressionModeExample
{
    private const string sampleMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    // change this path to the the location of your samples
    public static string pathToSamples = "C:/Users/AndreyAstashev/source/repos/GZipTest/GZipTest/Samples";

    private const string OriginalFileName = "original.txt";
    private const string CompressedFileName = "compressed.gz";
    private const string DecompressedFileName = "decompressed.txt";
    public static double originalFilesSize = 0;
    public static double totalSize = 0;

    public static void Main()
    {

        
        string[] files =
            Directory.GetFiles(pathToSamples, "*", SearchOption.AllDirectories);
        Console.WriteLine(" /////// ");
        Console.WriteLine(pathToSamples);
        Console.WriteLine(" /////// ");
        foreach (var file in files)
        {
            string fileName = pathToSamples + "/" + Path.GetFileName(file);
            Console.WriteLine(fileName);
            CompressFileAlt(fileName);
            DecompressFile();
            PrintResultsAlt(fileName);
            DeleteFilesAlt();
            Console.WriteLine(" ------------------------ ");

        }

        Console.WriteLine(" /////// ");
        Console.WriteLine("Original Size: " + originalFilesSize + " bytes");
        Console.WriteLine("Total Compressed Size: " + totalSize + " bytes");
        Console.WriteLine(" /////// ");
        Console.WriteLine(" Creating Dynamic Tree from the objects in the samples folder to fill the payload ");
        Console.WriteLine(" /////// ");

        // DynamicTreeItem is a recursive object to grab an entire directory and its subdirectories into json
        DynamicTreeItem dti = new DynamicTreeItem(new DirectoryInfo(@pathToSamples));
        string result = "[" + dti.JsonToDynatree() + "]";

        CreateFileToCompress2(result);
        CompressFile();
        DecompressFile();
        PrintResults();
        DeleteFiles();

    }

    private static void CreateFileToCompress() => File.WriteAllText(OriginalFileName, sampleMessage);

    private static void CreateFileToCompress2(String message) => File.WriteAllText(OriginalFileName, message);


    private static void CompressFile()
    {
        using FileStream originalFileStream = File.Open(OriginalFileName, FileMode.Open);
        using FileStream compressedFileStream = File.Create(CompressedFileName);
        using var compressor = new GZipStream(compressedFileStream, CompressionMode.Compress);
        originalFileStream.CopyTo(compressor);
    }

    private static void CompressFileAlt(string fileName)
    {
        using FileStream originalFileStream = File.Open(fileName, FileMode.Open);
        using FileStream compressedFileStream = File.Create(CompressedFileName);
        using var compressor = new GZipStream(compressedFileStream, CompressionMode.Compress);
        originalFileStream.CopyTo(compressor);
        
    }

    private static void DecompressFile()
    {
        using FileStream compressedFileStream = File.Open(CompressedFileName, FileMode.Open);
        using FileStream outputFileStream = File.Create(DecompressedFileName);
        using var decompressor = new GZipStream(compressedFileStream, CompressionMode.Decompress);
        decompressor.CopyTo(outputFileStream);
    }

    private static void DecompressFileAlt(string fileName)
    {
        using FileStream compressedFileStream = File.Open(CompressedFileName, FileMode.Open);
        using FileStream outputFileStream = File.Create(fileName);
        using var decompressor = new GZipStream(compressedFileStream, CompressionMode.Decompress);
        decompressor.CopyTo(outputFileStream);
    }

    private static void PrintResults()
    {
        double originalSize = new FileInfo(OriginalFileName).Length;
        double compressedSize = new FileInfo(CompressedFileName).Length;
        double decompressedSize = new FileInfo(DecompressedFileName).Length;

        Console.WriteLine($"The original file '{OriginalFileName}' weighs {originalSize} bytes.");
        Console.WriteLine($"The compressed file '{CompressedFileName}' weighs {compressedSize} bytes.");
        Console.WriteLine($"The decompressed file '{DecompressedFileName}' weighs {decompressedSize} bytes.");
        Console.WriteLine($"Compression " + ((decompressedSize - compressedSize) / decompressedSize));
        //Contents: \"{File.ReadAllText(DecompressedFileName)}\"
    }

    private static void PrintResultsAlt(string fileName)
    {
        double originalSize = new FileInfo(fileName).Length;
        double compressedSize = new FileInfo(CompressedFileName).Length;
        double decompressedSize = new FileInfo(DecompressedFileName).Length;

        Console.WriteLine($"The original file '{fileName}' weighs {originalSize} bytes.");
        Console.WriteLine($"The compressed file '{CompressedFileName}' weighs {compressedSize} bytes.");
        Console.WriteLine($"The decompressed file '{DecompressedFileName}' weighs {decompressedSize} bytes.");
        Console.WriteLine($"Compression " + ((decompressedSize - compressedSize) / decompressedSize));
        originalFilesSize = originalFilesSize + decompressedSize;
        totalSize = totalSize + compressedSize;


    }

    private static void DeleteFiles()
    {
        File.Delete(OriginalFileName);
        File.Delete(CompressedFileName);
        File.Delete(DecompressedFileName);
    }

    private static void DeleteFilesAlt()
    {
        File.Delete(CompressedFileName);
        File.Delete(DecompressedFileName);
    }

    class DynamicTreeItem
    {
        public string title { get; set; }
        public bool isFolder { get; set; }
        public byte[] content { get; set; }
        public string key { get; set; }
        public List<DynamicTreeItem> children;

        public DynamicTreeItem(FileSystemInfo fsi)
        {
            title = fsi.Name;
            children = new List<DynamicTreeItem>();

            if (fsi.Attributes == FileAttributes.Directory)
            {
                isFolder = true;
                foreach (FileSystemInfo f in (fsi as DirectoryInfo).GetFileSystemInfos())
                {
                    children.Add(new DynamicTreeItem(f));
                }
            }
            else
            {
                isFolder = false;
                content = ReadFully(File.Open(pathToSamples + "/" + fsi.Name, FileMode.Open));
            }
            key = title.Replace(" ", "").ToLower();
            
        }

        public string JsonToDynatree()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }


}